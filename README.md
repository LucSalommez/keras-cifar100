
# About
This project aims at building and training a basic Deep Learning classifier on the CIFAR-10 dataset using the Keras framework with Tensorflow backend.

 # Dependencies :

- Python 2.7+
- Tensorflow (https://www.tensorflow.org/install/)
- Keras, `pip install keras`
- Matplotlib, `pip install matplotlib`
- Pandas `pip install pandas`

# Training :

In order to train the model you must run the following command : `python train.py`
After each epoch the model last model will be stored under the `checkpoints` folder.

When you run `python train.py` it will try to find a model in that folder, if none exists, it will start the training from the beginning.

Logs of the training can be found in the `training.log` file

# Results :
![Accuracy](/results/accuracy.png?raw=true "Accuracy")
![Loss](/results/loss.png?raw=true "Loss")

As we can see on the plots, the model suffers a high bias as the approximate Bayes error on CIFAR-10 is only a maximum of few percents while we reach 14% on the training set.

We also can notice there is a high variance issue as the distance between test accuracy and the training one is pretty big (8%).


# Conclusion :

As I could only train the model with my MacBook Pro on CPU, I made the choice to use a simple model which is the reason why it only reached an accuracy of 86% on training set and 78% on the test set.

Still the project is a success as it’s purpose was to show a simple implementation of a neural net with Keras and as 78% accuracy still is far better than random guessing (10%).

Further improvement could be done in order to reduce the bias and the variance of the model.
A few ideas to improve the model could be :
- Having a bigger model
- Using BatchNorm
- Using the ELU activation function instead of the RELU one in order to speed up training and limit the number of dead neurons
- Adding regularisation, even though the model already uses Dropout
- Doing data augmentation
