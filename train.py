import numpy as np
import os.path

from keras.datasets import cifar10
from keras.utils import np_utils
from keras.models import load_model
from keras.callbacks import (
    ModelCheckpoint,
    CSVLogger,
)

from model import base_model

# Load the dataset, randomly shuffled and split between train and test set
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

# Meta parameters
nb_classes = 10
batch_size = 32
nb_epochs = 30

# Name of the file containing the model
model_filename = "checkpoints/weights.hdf5"

# Visualization of 10 random examples from the training set, one for each class
should_display = False
if (should_display == True) :
    fig = plt.figure(figsize=(10,5))
    for i in range(nb_classes) :
        # Add a new plot
        ax = fig.add_subplot(2, 5, 1 + i, xticks=[], yticks=[])

        # Get an array of all indexes corresponding to the ith class
        idx = np.where(y_train[:]==i)[0]

        # Choose a random index in this array
        img_num = np.random.randint(idx.shape[0])

        # Get the corresponding image in the training set for that index
        im = x_train[idx[img_num],::]

        # Set a title to the plot and draw the image
        ax.set_title(i)
        plt.imshow(im)

    # Display the images
    plt.show()

# Convert targets into one hot vectors (ex : [3] -> [0 0 1 0 0 0 0 0 0 0])
y_train = np_utils.to_categorical(y_train, nb_classes)
y_test = np_utils.to_categorical(y_test, nb_classes)

# Convert the inputs to be of type float32
x_train = x_train.astype("float32")
x_test = x_test.astype("float32")

# Normalize the inputs
mean_img = np.mean(x_train, axis=0)
x_train = (x_train - mean_img) / 128
x_test = (x_test - mean_img) / 128

# Create the model and display a summary of its structure and params
if (os.path.isfile(model_filename)) :
    mod = load_model(model_filename)
else :
    mod = base_model(x_train.shape[1:], nb_classes)

mod.summary()

# Define callbacks, one to save the model after each epoch and the other for logs
model_checkpoint = ModelCheckpoint(model_filename,
                    monitor='val_loss',
                    verbose=0, save_best_only=False,
                    save_weights_only=False, mode='auto',
                    period=1)
csv_logger = CSVLogger('training.log', append=True)

# Start the training
history = mod.fit(x_train, y_train,
                batch_size=batch_size,
                epochs=nb_epochs,
                validation_data=(x_test,y_test),
                shuffle=True,
                callbacks=[model_checkpoint, csv_logger],
                initial_epoch=7)
