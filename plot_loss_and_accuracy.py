import matplotlib.pyplot as plt
from pandas import read_csv

data = read_csv('training.log')

# Plot training & validation accuracy values
plt.plot(data['acc'])
plt.plot(data['val_acc'])
plt.title('Model accuracy')
plt.ylabel('Accuracy')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()

# Plot training & validation loss values
plt.plot(data['loss'])
plt.plot(data['val_loss'])
plt.title('Model loss')
plt.ylabel('Loss')
plt.xlabel('Epoch')
plt.legend(['Train', 'Test'], loc='upper left')
plt.show()
